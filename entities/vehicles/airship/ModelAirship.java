//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class unknown extends ModelBase {
	private final ModelRenderer bone;
	private final ModelRenderer prop1;
	private final ModelRenderer prop2;
	private final ModelRenderer bone2;

	public unknown() {
		textureWidth = 64;
		textureHeight = 64;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 24, -2.0F, -4.0F, -4.0F, 4, 4, 8, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 16, 43, -6.0F, -2.5F, -3.0F, 4, 1, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 16, 45, 2.0F, -2.5F, -3.0F, 4, 1, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -4.0F, -12.0F, -8.0F, 8, 8, 16, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 18, 36, -3.0F, -11.0F, 8.0F, 6, 6, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 32, 36, -3.0F, -11.0F, -9.0F, 6, 6, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 36, 4.0F, -8.0F, 4.0F, 3, 1, 6, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 24, 24, -7.0F, -8.0F, 4.0F, 3, 1, 6, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 43, -0.5F, -14.0F, 4.0F, 1, 2, 3, 0.0F, false));

		prop1 = new ModelRenderer(this);
		prop1.setRotationPoint(4.5F, 22.0F, -1.75F);
		prop1.cubeList.add(new ModelBox(prop1, 0, 48, -1.5F, -1.5F, 0.0F, 3, 3, 0, 0.0F, false));

		prop2 = new ModelRenderer(this);
		prop2.setRotationPoint(-4.5F, 22.0F, -1.75F);
		prop2.cubeList.add(new ModelBox(prop2, 26, 43, -1.5F, -1.5F, 0.0F, 3, 3, 0, 0.0F, false));

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 11.0F, 7.5F);
		bone2.cubeList.add(new ModelBox(bone2, 8, 43, -0.5F, -1.0F, -0.5F, 1, 2, 3, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
		prop1.render(f5);
		prop2.render(f5);
		bone2.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}