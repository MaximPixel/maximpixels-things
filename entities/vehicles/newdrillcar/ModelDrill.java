//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class modeldrill extends ModelBase {
	private final ModelRenderer bone;
	private final ModelRenderer wheel1;
	private final ModelRenderer wheel2;
	private final ModelRenderer wheel3;
	private final ModelRenderer wheel4;
	private final ModelRenderer bone2;
	private final ModelRenderer door1;
	private final ModelRenderer door2;

	public modeldrill() {
		textureWidth = 208;
		textureHeight = 208;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 118, 0, -11.0F, -9.0F, -10.0F, 22, 7, 20, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 70, 40, -7.0F, -9.0F, 10.0F, 14, 7, 10, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 118, 40, -7.0F, -9.0F, -20.0F, 14, 7, 10, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 76, 63, -10.0F, -11.0F, -21.0F, 20, 10, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -10.0F, -10.0F, -20.0F, 20, 1, 39, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 40, -10.0F, -18.0F, -17.0F, 20, 8, 15, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 82, -10.0F, -28.0F, 10.0F, 1, 18, 9, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 20, 82, 9.0F, -28.0F, 10.0F, 1, 18, 9, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 72, 109, -10.0F, -28.0F, -7.0F, 1, 10, 5, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 84, 109, 9.0F, -28.0F, -7.0F, 1, 10, 5, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 63, -9.0F, -28.0F, 18.0F, 18, 18, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 118, 63, -9.0F, -28.0F, -6.0F, 18, 10, 1, 0.0F, false));

		wheel1 = new ModelRenderer(this);
		wheel1.setRotationPoint(9.0F, 20.5F, 15.0F);
		wheel1.cubeList.add(new ModelBox(wheel1, 18, 109, -1.0F, -3.5F, -3.5F, 2, 7, 7, 0.0F, false));

		wheel2 = new ModelRenderer(this);
		wheel2.setRotationPoint(9.0F, 20.5F, -15.0F);
		wheel2.cubeList.add(new ModelBox(wheel2, 0, 109, -1.0F, -3.5F, -3.5F, 2, 7, 7, 0.0F, false));

		wheel3 = new ModelRenderer(this);
		wheel3.setRotationPoint(-9.0F, 20.5F, -15.0F);
		wheel3.cubeList.add(new ModelBox(wheel3, 36, 109, -1.0F, -3.5F, -3.5F, 2, 7, 7, 0.0F, false));

		wheel4 = new ModelRenderer(this);
		wheel4.setRotationPoint(-9.0F, 20.5F, 15.0F);
		wheel4.cubeList.add(new ModelBox(wheel4, 54, 109, -1.0F, -3.5F, -3.5F, 2, 7, 7, 0.0F, false));
		wheel4.cubeList.add(new ModelBox(wheel4, 0, 124, -1.0F, -1.0F, 0.0F, 1, 1, 1, 0.0F, false));

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 15.0F, 24.5F);
		setRotationAngle(bone2, 0.4363F, 0.0F, 0.0F);
		bone2.cubeList.add(new ModelBox(bone2, 38, 63, -5.0F, -3.0F, -6.5F, 10, 6, 9, 0.0F, false));

		door1 = new ModelRenderer(this);
		door1.setRotationPoint(0.0F, 24.0F, 0.0F);
		door1.cubeList.add(new ModelBox(door1, 66, 82, 9.0F, -17.0F, -2.0F, 1, 7, 12, 0.0F, false));

		door2 = new ModelRenderer(this);
		door2.setRotationPoint(0.0F, 24.0F, 0.0F);
		door2.cubeList.add(new ModelBox(door2, 40, 82, -10.0F, -17.0F, -2.0F, 1, 7, 12, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
		wheel1.render(f5);
		wheel2.render(f5);
		wheel3.render(f5);
		wheel4.render(f5);
		bone2.render(f5);
		door1.render(f5);
		door2.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}