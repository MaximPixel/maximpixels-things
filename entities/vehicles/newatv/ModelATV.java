//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class ModelATV extends ModelBase {
	private final ModelRenderer base;
	private final ModelRenderer bone5;
	private final ModelRenderer bone;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;
	private final ModelRenderer bone6;

	public ModelATV() {
		textureWidth = 128;
		textureHeight = 128;

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 24.0F, 0.0F);
		base.cubeList.add(new ModelBox(base, 0, 0, -7.0F, -4.0F, -5.0F, 14, 1, 10, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 48, 0, -4.5F, -9.5F, -12.5F, 9, 6, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 32, 14, -4.5F, -9.5F, 4.5F, 9, 6, 7, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 14, -3.0F, -9.0F, -5.0F, 6, 5, 10, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 43, 4.5F, -8.5F, -12.5F, 4, 1, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 24, 43, -8.5F, -8.5F, -12.5F, 4, 1, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 48, 43, -8.5F, -8.5F, 4.5F, 4, 1, 8, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 52, 4.5F, -8.5F, 4.5F, 4, 1, 8, 0.0F, false));

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.0F, 14.5F, -12.5F);
		setRotationAngle(bone5, -0.2618F, 0.0F, 0.0F);
		bone5.cubeList.add(new ModelBox(bone5, 24, 52, -4.0F, 0.4659F, -0.2412F, 8, 6, 1, 0.0F, false));

		bone = new ModelRenderer(this);
		bone.setRotationPoint(-6.0F, 20.5F, 8.5F);
		bone.cubeList.add(new ModelBox(bone, 0, 29, -2.0F, -3.5F, -3.5F, 4, 7, 7, 0.0F, false));

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(-6.0F, 20.5F, -8.5F);
		bone2.cubeList.add(new ModelBox(bone2, 22, 29, -2.0F, -3.5F, -3.5F, 4, 7, 7, 0.0F, false));

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(6.0F, 20.5F, -8.5F);
		bone3.cubeList.add(new ModelBox(bone3, 44, 29, -2.0F, -3.5F, -3.5F, 4, 7, 7, 0.0F, false));

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(6.0F, 20.5F, 8.5F);
		bone4.cubeList.add(new ModelBox(bone4, 64, 14, -2.0F, -3.5F, -3.5F, 4, 7, 7, 0.0F, false));

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(-0.5F, 15.0F, -9.5F);
		setRotationAngle(bone6, -0.7854F, 0.0F, 0.0F);
		bone6.cubeList.add(new ModelBox(bone6, 44, 52, -0.5F, -3.5F, -0.5F, 1, 4, 1, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 24, 59, -4.5F, -4.5F, -0.5F, 9, 1, 1, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		base.render(f5);
		bone5.render(f5);
		bone.render(f5);
		bone2.render(f5);
		bone3.render(f5);
		bone4.render(f5);
		bone6.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}