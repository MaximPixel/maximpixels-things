//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class tank extends ModelBase {
	private final ModelRenderer tracks;

	public tank() {
		textureWidth = 176;
		textureHeight = 176;

		tracks = new ModelRenderer(this);
		tracks.setRotationPoint(0.0F, 24.0F, 0.0F);
		tracks.cubeList.add(new ModelBox(tracks, 80, 0, -2.5F, -7.0F, -17.0F, 5, 7, 34, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		tracks.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}