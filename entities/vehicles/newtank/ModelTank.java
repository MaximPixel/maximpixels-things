//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class tank extends ModelBase {
	private final ModelRenderer base;
	private final ModelRenderer bone;
	private final ModelRenderer bone2;
	private final ModelRenderer tracks;

	public tank() {
		textureWidth = 176;
		textureHeight = 176;

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 24.0F, 0.0F);
		base.cubeList.add(new ModelBox(base, 0, 45, -7.0F, -8.0F, -19.0F, 14, 6, 38, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 1, -10.0F, -12.0F, -20.0F, 20, 4, 40, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 32, 111, -6.0F, -18.0F, 1.0F, 12, 2, 12, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 66, 45, -9.0F, -16.0F, -12.0F, 18, 4, 31, 0.0F, false));

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 2.0F, 7.0F);
		bone.cubeList.add(new ModelBox(bone, 48, 89, -7.0F, 3.0F, -7.0F, 14, 1, 14, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 89, -8.0F, -3.0F, -8.0F, 16, 6, 16, 0.0F, false));

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 0.0F, -5.0F);
		bone.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 0, 111, -2.0F, -2.0F, -22.0F, 4, 4, 24, 0.0F, false));

		tracks = new ModelRenderer(this);
		tracks.setRotationPoint(0.0F, 24.0F, 0.0F);
		tracks.cubeList.add(new ModelBox(tracks, 80, 0, 7.0F, -7.0F, -17.0F, 5, 7, 34, 0.0F, false));
		tracks.cubeList.add(new ModelBox(tracks, 70, 80, -12.0F, -7.0F, -17.0F, 5, 7, 34, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		base.render(f5);
		bone.render(f5);
		tracks.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}