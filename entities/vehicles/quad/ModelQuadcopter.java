//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class quad extends ModelBase {
	private final ModelRenderer bone;
	private final ModelRenderer bone2;
	private final ModelRenderer bone6;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;
	private final ModelRenderer bone5;
	private final ModelRenderer bone7;
	private final ModelRenderer bone8;
	private final ModelRenderer bone9;

	public quad() {
		textureWidth = 48;
		textureHeight = 48;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 0, -2.0F, -2.0F, -3.0F, 4, 2, 6, 0.0F, false));

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(bone2, 0.0F, -0.7854F, 0.0F);
		bone2.cubeList.add(new ModelBox(bone2, 0, 10, -8.5F, -1.2F, -0.5F, 7, 1, 1, 0.0F, false));

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(-8.0F, -1.0F, 0.0F);
		bone2.addChild(bone6);
		bone6.cubeList.add(new ModelBox(bone6, 8, 12, -1.5F, -0.5F, -0.5F, 3, 0, 1, 0.0F, false));

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(bone3, 0.0F, 0.7854F, 0.0F);
		bone3.cubeList.add(new ModelBox(bone3, 16, 8, -8.5F, -1.2F, -0.5F, 7, 1, 1, 0.0F, false));

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(-8.0F, -1.0F, 0.0F);
		bone3.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 0, 12, -1.5F, -0.5F, -0.5F, 3, 0, 1, 0.0F, false));

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(bone5, 0.0F, -2.3562F, 0.0F);
		bone5.cubeList.add(new ModelBox(bone5, 0, 8, -8.5F, -1.2F, -0.5F, 7, 1, 1, 0.0F, false));

		bone7 = new ModelRenderer(this);
		bone7.setRotationPoint(-8.0F, -1.0F, 0.0F);
		bone5.addChild(bone7);
		bone7.cubeList.add(new ModelBox(bone7, 16, 11, -1.5F, -0.5F, -0.5F, 3, 0, 1, 0.0F, false));

		bone8 = new ModelRenderer(this);
		bone8.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(bone8, 0.0F, 2.3562F, 0.0F);
		bone8.cubeList.add(new ModelBox(bone8, 20, 0, -8.5F, -1.2F, -0.5F, 7, 1, 1, 0.0F, false));

		bone9 = new ModelRenderer(this);
		bone9.setRotationPoint(-8.0F, -1.0F, 0.0F);
		bone8.addChild(bone9);
		bone9.cubeList.add(new ModelBox(bone9, 16, 10, -1.5F, -0.5F, -0.5F, 3, 0, 1, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
		bone2.render(f5);
		bone3.render(f5);
		bone5.render(f5);
		bone8.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}