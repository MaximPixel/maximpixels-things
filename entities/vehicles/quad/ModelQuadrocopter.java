//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class ModelQuadrocopter extends ModelBase {
	private final ModelRenderer bone;
	private final ModelRenderer bone;
	private final ModelRenderer prop1;
	private final ModelRenderer prop2;
	private final ModelRenderer prop3;
	private final ModelRenderer prop4;
	private final ModelRenderer bone;
	private final ModelRenderer bone;
	private final ModelRenderer bone;
	private final ModelRenderer bone;

	public ModelQuadrocopter() {
		textureWidth = 128;
		textureHeight = 64;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 0, -2.5F, -7.25F, -3.0F, 5, 2, 6, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -7.0F, -6.5F, 5.0F, 2, 1, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, 5.0F, -6.5F, -7.0F, 2, 1, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -7.0F, -6.5F, -7.0F, 2, 1, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, 5.0F, -6.5F, 5.0F, 2, 1, 2, 0.0F, false));

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(bone, 0.0F, 0.7854F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 0, -8.5F, -6.0F, -0.5F, 17, 1, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -0.5F, -5.9F, -8.5F, 1, 1, 17, 0.0F, false));

		prop1 = new ModelRenderer(this);
		prop1.setRotationPoint(6.0F, 17.25F, -6.0F);
		prop1.cubeList.add(new ModelBox(prop1, 0, 0, -2.0F, 0.0F, -2.0F, 4, 0, 4, 0.0F, false));

		prop2 = new ModelRenderer(this);
		prop2.setRotationPoint(-6.0F, 17.25F, 6.0F);
		prop2.cubeList.add(new ModelBox(prop2, 0, 0, -2.0F, 0.0F, -2.0F, 4, 0, 4, 0.0F, false));

		prop3 = new ModelRenderer(this);
		prop3.setRotationPoint(-6.0F, 17.25F, -6.0F);
		prop3.cubeList.add(new ModelBox(prop3, 0, 0, -2.0F, 0.0F, -2.0F, 4, 0, 4, 0.0F, false));

		prop4 = new ModelRenderer(this);
		prop4.setRotationPoint(6.0F, 17.25F, 6.0F);
		prop4.cubeList.add(new ModelBox(prop4, 0, 0, -2.0F, 0.0F, -2.0F, 4, 0, 4, 0.0F, false));

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 19.5F, 0.0F);
		setRotationAngle(bone, 0.0F, 0.0F, -0.8727F);
		bone.cubeList.add(new ModelBox(bone, 0, 0, -5.0F, -1.0F, -1.5F, 5, 1, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -5.0F, -1.0F, 0.5F, 5, 1, 1, 0.0F, false));

		bone = new ModelRenderer(this);
		bone.setRotationPoint(-5.5F, 19.0F, 0.0F);
		setRotationAngle(bone, 0.0F, 0.0F, -0.6981F);
		bone.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 0, 0, -0.5F, -0.5F, -4.0F, 1, 1, 8, 0.0F, false));

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 19.5F, 0.0F);
		setRotationAngle(bone, 0.0F, 0.0F, 0.8727F);
		bone.cubeList.add(new ModelBox(bone, 0, 0, 0.0F, -1.0F, -1.5F, 5, 1, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, 0.0F, -1.0F, 0.5F, 5, 1, 1, 0.0F, false));

		bone = new ModelRenderer(this);
		bone.setRotationPoint(5.5F, 19.0F, 0.0F);
		setRotationAngle(bone, 0.0F, 0.0F, 0.6981F);
		bone.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 0, 0, -0.5F, -0.5F, -4.0F, 1, 1, 8, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
		bone.render(f5);
		prop1.render(f5);
		prop2.render(f5);
		prop3.render(f5);
		prop4.render(f5);
		bone.render(f5);
		bone.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}