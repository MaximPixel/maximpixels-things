//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class ModelDrill extends ModelBase {
	private final ModelRenderer bone;

	public ModelDrill() {
		textureWidth = 144;
		textureHeight = 144;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 0, -9.0F, -1.0F, -9.0F, 18, 1, 18, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 72, 0, -8.0F, -2.0F, -8.0F, 16, 1, 16, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 19, -7.0F, -3.0F, -7.0F, 14, 1, 14, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 56, 19, -6.0F, -4.0F, -6.0F, 12, 1, 12, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 34, -5.0F, -5.0F, -5.0F, 10, 1, 10, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 40, 34, -4.0F, -6.0F, -4.0F, 8, 1, 8, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 72, 34, -3.0F, -7.0F, -3.0F, 6, 1, 6, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 45, -2.0F, -8.0F, -2.0F, 4, 1, 4, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 16, 45, -1.0F, -9.0F, -1.0F, 2, 1, 2, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}