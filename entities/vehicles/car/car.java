//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class unknown extends ModelBase {
	private final ModelRenderer base;
	private final ModelRenderer backLeftWheel;
	private final ModelRenderer backRightWheel;
	private final ModelRenderer upLeftWheel;
	private final ModelRenderer upRightWheel;
	private final ModelRenderer glass;
	private final ModelRenderer leftDoor;
	private final ModelRenderer rightDoor;

	public unknown() {
		textureWidth = 176;
		textureHeight = 176;

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 24.0F, 0.0F);
		base.cubeList.add(new ModelBox(base, 0, 0, -8.0F, -3.0F, -16.0F, 16, 1, 32, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 102, 8.0F, -13.0F, -16.0F, 1, 11, 10, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 98, 81, -9.0F, -13.0F, -16.0F, 1, 11, 10, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 28, 57, -9.0F, -13.0F, -17.0F, 18, 11, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 28, 69, -9.0F, -12.0F, 16.0F, 18, 10, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 56, 33, -8.0F, -13.0F, -16.0F, 16, 1, 10, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 22, 102, -8.0F, -13.0F, 13.0F, 16, 1, 3, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 66, 81, -8.0F, -25.0F, 13.5F, 16, 12, 0, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 76, 123, 8.0F, -25.0F, 13.0F, 1, 12, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 72, 123, -9.0F, -25.0F, 13.0F, 1, 12, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 96, 0, -9.0F, -26.0F, -4.0F, 18, 1, 18, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 60, 102, -9.0F, -13.0F, 7.0F, 1, 11, 9, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 80, 102, 8.0F, -13.0F, 7.0F, 1, 11, 9, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 32, 81, -8.0F, -12.0F, -7.0F, 16, 9, 1, 0.0F, false));

		backLeftWheel = new ModelRenderer(this);
		backLeftWheel.setRotationPoint(9.5F, 20.5F, 12.5F);
		backLeftWheel.cubeList.add(new ModelBox(backLeftWheel, 54, 123, -1.0F, -3.5F, -3.5F, 2, 7, 7, 0.0F, false));

		backRightWheel = new ModelRenderer(this);
		backRightWheel.setRotationPoint(-9.5F, 20.5F, 12.5F);
		backRightWheel.cubeList.add(new ModelBox(backRightWheel, 36, 123, -1.0F, -3.5F, -3.5F, 2, 7, 7, 0.0F, false));

		upLeftWheel = new ModelRenderer(this);
		upLeftWheel.setRotationPoint(9.5F, 20.5F, -11.5F);
		upLeftWheel.cubeList.add(new ModelBox(upLeftWheel, 18, 123, -1.0F, -3.5F, -3.5F, 2, 7, 7, 0.0F, false));

		upRightWheel = new ModelRenderer(this);
		upRightWheel.setRotationPoint(-9.5F, 20.5F, -11.5F);
		upRightWheel.cubeList.add(new ModelBox(upRightWheel, 0, 123, -1.0F, -3.5F, -3.5F, 2, 7, 7, 0.0F, false));

		glass = new ModelRenderer(this);
		glass.setRotationPoint(7.5F, 4.5F, -3.5F);
		setRotationAngle(glass, 1.0472F, 0.0F, 0.0F);
		glass.cubeList.add(new ModelBox(glass, 0, 81, -0.5F, -1.5F, -9.0F, 1, 1, 15, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 66, 57, -15.5F, -1.5F, -9.0F, 1, 1, 15, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 0, 33, -14.5F, -0.5F, -8.0F, 14, 0, 14, 0.0F, false));

		leftDoor = new ModelRenderer(this);
		leftDoor.setRotationPoint(0.0F, 24.0F, 0.0F);
		leftDoor.cubeList.add(new ModelBox(leftDoor, 108, 33, 8.0F, -13.0F, -6.0F, 1, 11, 13, 0.0F, false));

		rightDoor = new ModelRenderer(this);
		rightDoor.setRotationPoint(0.0F, 24.0F, 0.0F);
		rightDoor.cubeList.add(new ModelBox(rightDoor, 0, 57, -9.0F, -13.0F, -6.0F, 1, 11, 13, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		base.render(f5);
		backLeftWheel.render(f5);
		backRightWheel.render(f5);
		upLeftWheel.render(f5);
		upRightWheel.render(f5);
		glass.render(f5);
		leftDoor.render(f5);
		rightDoor.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}