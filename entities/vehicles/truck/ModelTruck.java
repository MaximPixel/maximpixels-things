//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class ModelCar extends ModelBase {
	private final ModelRenderer bone;
	private final ModelRenderer bone2;

	public ModelCar() {
		textureWidth = 160;
		textureHeight = 160;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 34, 52, -9.0F, -1.0F, -19.0F, 18, 1, 5, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 66, -6.0F, -1.0F, -14.0F, 12, 1, 7, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 26, -9.0F, -1.0F, -7.0F, 18, 1, 16, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, -6.0F, -1.0F, 9.0F, 12, 1, 7, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 34, 46, -9.0F, -1.0F, 16.0F, 18, 1, 5, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 74, 5.0F, -5.0F, 8.0F, 1, 4, 9, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 20, 74, -6.0F, -5.0F, 8.0F, 1, 4, 9, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 60, 74, -6.0F, -5.0F, -15.0F, 1, 4, 9, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 40, 74, 5.0F, -5.0F, -15.0F, 1, 4, 9, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 68, 26, 8.0F, -5.0F, -7.0F, 1, 4, 16, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 46, -9.0F, -5.0F, -7.0F, 1, 4, 16, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 80, 74, 8.0F, -5.0F, -19.0F, 1, 4, 5, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 87, 8.0F, -5.0F, 16.0F, 1, 4, 5, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 24, 87, -9.0F, -5.0F, 16.0F, 1, 4, 5, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 12, 87, -9.0F, -5.0F, -19.0F, 1, 4, 5, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 76, 66, -8.0F, -5.0F, -19.0F, 16, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 110, 66, -8.0F, -5.0F, 20.0F, 16, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 36, 87, 6.0F, -5.0F, 16.0F, 2, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 60, 87, 6.0F, -5.0F, 8.0F, 2, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 42, 87, 6.0F, -5.0F, -7.0F, 2, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 72, 87, 6.0F, -5.0F, -15.0F, 2, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 48, 87, -8.0F, -5.0F, 16.0F, 2, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 96, -8.0F, -5.0F, 8.0F, 2, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 54, 87, -8.0F, -5.0F, -7.0F, 2, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 66, 87, -8.0F, -5.0F, -15.0F, 2, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -10.0F, -6.0F, -3.0F, 20, 1, 25, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 90, 0, -9.0F, -6.0F, -20.0F, 18, 1, 17, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -10.0F, -26.0F, -3.0F, 1, 20, 25, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, 9.0F, -26.0F, -3.0F, 1, 20, 25, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -10.0F, -27.0F, -3.0F, 20, 1, 25, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 90, 0, -9.0F, -16.0F, -19.0F, 1, 10, 16, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 90, 0, 8.0F, -16.0F, -19.0F, 1, 10, 16, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 90, 0, -9.0F, -16.0F, -20.0F, 18, 10, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 90, 0, -9.0F, -26.0F, -20.0F, 18, 1, 17, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 90, 0, -9.0F, -26.0F, -3.0F, 18, 20, 1, 0.0F, false));

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, -1.5F, 21.5F);
		bone2.cubeList.add(new ModelBox(bone2, 90, 0, -9.0F, -0.5F, -0.5F, 18, 20, 1, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
		bone2.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}