//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class modelexcModelExcavatoravator extends ModelBase {
	private final ModelRenderer bone;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;

	public modelexcModelExcavatoravator() {
		textureWidth = 256;
		textureHeight = 256;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 0, -9.0F, -8.0F, -11.0F, 18, 1, 22, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 80, 0, -6.0F, -7.0F, -10.0F, 12, 6, 20, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 26, 8.0F, -14.0F, -11.0F, 1, 6, 22, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 46, 26, -9.0F, -14.0F, -11.0F, 1, 6, 22, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 98, -8.0F, -14.0F, -11.0F, 16, 6, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 105, -8.0F, -14.0F, 10.0F, 16, 6, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 124, -8.0F, -32.0F, 8.0F, 1, 24, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 46, 98, -8.0F, -32.0F, -10.0F, 1, 24, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 34, 98, 7.0F, -32.0F, -10.0F, 1, 24, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 40, 98, 7.0F, -32.0F, 8.0F, 1, 24, 2, 0.0F, false));

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone2.cubeList.add(new ModelBox(bone2, 92, 26, 6.0F, -5.0F, -9.0F, 3, 5, 18, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 0, 54, -9.0F, -5.0F, -9.0F, 3, 5, 18, 0.0F, false));

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.0F, 10.0F, 7.0F);
		bone3.cubeList.add(new ModelBox(bone3, 98, 54, 9.0F, -1.0F, -18.0F, 1, 2, 19, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 0, 77, -10.0F, -1.0F, -18.0F, 1, 2, 19, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 40, 77, -11.0F, 7.0F, -20.0F, 22, 7, 1, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 42, 54, -11.0F, 13.0F, -26.0F, 22, 1, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 6, 134, 10.0F, 8.0F, -25.0F, 1, 5, 5, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 6, 124, -11.0F, 8.0F, -25.0F, 1, 5, 5, 0.0F, false));

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(0.0F, -0.5F, -17.5F);
		setRotationAngle(bone4, 1.3963F, 0.0F, 0.0F);
		bone3.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 86, 77, 9.0F, -0.5F, -14.5F, 1, 2, 15, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 118, 77, -10.0F, -0.5F, -14.5F, 1, 2, 15, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
		bone2.render(f5);
		bone3.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}