//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class  extends ModelBase {
	private final ModelRenderer bone;
	private final ModelRenderer bone2;
	private final ModelRenderer bone4;

	public () {
		textureWidth = 256;
		textureHeight = 256;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 228, -13.0F, -5.0F, -12.0F, 4, 5, 23, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 228, 9.0F, -5.0F, -12.0F, 4, 5, 23, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 29, -9.0F, -6.0F, -13.0F, 18, 4, 27, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -11.0F, -7.0F, -14.0F, 22, 1, 28, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 22, 60, -4.0F, -14.0F, -15.0F, 9, 7, 8, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 60, -11.0F, -19.0F, -7.0F, 1, 12, 20, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 60, 10.0F, -19.0F, -7.0F, 1, 12, 20, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 92, -10.0F, -19.0F, -7.0F, 20, 12, 1, 0.0F, false));

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 12.5F, -11.5F);
		setRotationAngle(bone2, -0.8727F, 0.0F, 0.0F);
		bone2.cubeList.add(new ModelBox(bone2, 0, 105, -11.0F, 2.5F, -6.5F, 22, 9, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 0, 116, -11.0F, 10.5F, -10.5F, 22, 1, 4, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 42, 80, -11.0F, 5.5F, -9.5F, 1, 5, 3, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 42, 80, 10.0F, 5.5F, -9.5F, 1, 5, 3, 0.0F, false));

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone4, 0.8727F, 0.0F, 0.0F);
		bone2.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 42, 88, 5.0F, -1.5F, -7.5F, 2, 2, 8, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 42, 88, -6.0F, -1.5F, -7.5F, 2, 2, 8, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
		bone2.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}