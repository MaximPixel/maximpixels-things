//Made with Blockbench
//Paste this code into your mod. Don't forget to add your imports

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class rcelicopter extends ModelBase {
	public ModelRenderer bone1;
	public ModelRenderer bone2;
	public ModelRenderer bone3;
	public ModelRenderer bone4;
	public ModelRenderer bone5;

	public rcelicopter() {
		textureWidth = 64;
		textureHeight = 64;

		bone1 = new ModelRenderer(this);
		bone1.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone1.cubeList.add(new ModelBox(bone1, 0, 12, -2.0F, -6.0F, -4.0F, 4, 4, 6, 0.0F, true));
		bone1.cubeList.add(new ModelBox(bone1, 20, 12, -0.5F, -6.0F, 2.0F, 1, 2, 8, 0.0F, false));
		bone1.cubeList.add(new ModelBox(bone1, 16, 22, 2.0F, -1.0F, -4.5F, 1, 1, 7, 0.0F, false));
		bone1.cubeList.add(new ModelBox(bone1, 0, 22, -3.0F, -1.0F, -4.5F, 1, 1, 7, 0.0F, false));
		bone1.cubeList.add(new ModelBox(bone1, 32, 30, -1.0F, -7.0F, -3.0F, 2, 1, 3, 0.0F, false));

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(3.0F, 24.0F, 0.0F);
		setRotationAngle(bone2, 0.0F, 0.0F, -0.4363F);
		bone2.cubeList.add(new ModelBox(bone2, 0, 38, -1.0F, -3.0F, 0.5F, 1, 2, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 8, 38, -1.0F, -3.0F, -3.5F, 1, 2, 1, 0.0F, false));

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(-3.0F, 24.0F, 0.0F);
		setRotationAngle(bone3, 0.0F, 0.0F, 0.4363F);
		bone3.cubeList.add(new ModelBox(bone3, 4, 38, 0.0F, -3.0F, 0.5F, 1, 2, 1, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 12, 38, 0.0F, -3.0F, -3.5F, 1, 2, 1, 0.0F, false));

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(0.0F, 19.0F, 9.5F);
		bone4.cubeList.add(new ModelBox(bone4, 22, 30, 0.5F, -2.0F, -2.5F, 1, 4, 4, 0.0F, false));

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.0F, 24.0F, -1.5F);
		bone5.cubeList.add(new ModelBox(bone5, 0, 30, -10.5F, -8.0F, -0.5F, 10, 1, 1, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 32, 22, 0.5F, -8.0F, -0.5F, 10, 1, 1, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 0, 0, -0.5F, -8.0F, -0.5F, 1, 1, 11, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 24, 0, -0.5F, -8.0F, -10.5F, 1, 1, 10, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) { 
		bone1.render(f5);
		bone2.render(f5);
		bone3.render(f5);
		bone4.render(f5);
		bone5.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}