//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class ModelRocket extends ModelBase {
	private final ModelRenderer bone;
	private final ModelRenderer bone2;

	public ModelRocket() {
		textureWidth = 256;
		textureHeight = 256;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 98, -5.0F, -2.0F, -5.0F, 10, 2, 10, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 72, 98, -4.0F, -4.0F, -4.0F, 8, 2, 8, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 14, 117, -3.0F, -6.0F, -3.0F, 6, 2, 6, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 24, 136, -2.0F, -7.0F, -2.0F, 4, 1, 4, 0.0F, false));

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone2.cubeList.add(new ModelBox(bone2, 64, 0, -8.0F, -8.0F, -8.0F, 16, 1, 16, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 0, 21, 8.0F, -37.0F, -8.0F, 1, 30, 16, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 34, 21, -9.0F, -37.0F, -8.0F, 1, 30, 16, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 0, 67, -8.0F, -37.0F, -9.0F, 16, 30, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 34, 67, -8.0F, -37.0F, 8.0F, 16, 30, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 0, 0, -8.0F, -41.0F, -8.0F, 16, 5, 16, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 68, 67, -6.0F, -46.0F, -6.0F, 12, 5, 12, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 40, 98, -4.0F, -50.0F, -4.0F, 8, 4, 8, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 12, 136, -1.5F, -56.0F, -1.5F, 3, 6, 3, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 38, 117, 9.0F, -13.0F, -0.5F, 6, 13, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 40, 136, 9.0F, -18.0F, -0.5F, 5, 5, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 74, 136, 9.0F, -19.0F, -0.5F, 4, 1, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 0, 146, -13.0F, -19.0F, -0.5F, 4, 1, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 52, 117, -15.0F, -13.0F, -0.5F, 6, 13, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 52, 136, -14.0F, -18.0F, -0.5F, 5, 5, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 0, 117, -0.5F, -13.0F, -15.0F, 1, 13, 6, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 66, 117, -0.5F, -18.0F, -14.0F, 1, 5, 5, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 64, 136, -0.5F, -19.0F, -13.0F, 1, 1, 4, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 104, 98, -0.5F, -13.0F, 9.0F, 1, 13, 6, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 0, 136, -0.5F, -18.0F, 9.0F, 1, 5, 5, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 64, 141, -0.5F, -19.0F, 9.0F, 1, 1, 4, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
		bone2.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}