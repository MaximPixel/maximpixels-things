//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class unknown extends ModelBase {
	private final ModelRenderer base;
	private final ModelRenderer legs1;
	private final ModelRenderer legs2;
	private final ModelRenderer head;

	public unknown() {
		textureWidth = 64;
		textureHeight = 64;

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 24.0F, 0.0F);
		base.cubeList.add(new ModelBox(base, 36, 22, -2.0F, -8.5F, -2.0F, 4, 1, 4, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 8, 39, -1.5F, -1.0F, 7.0F, 3, 1, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 0, 43, -1.5F, -1.0F, -8.0F, 3, 1, 1, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 8, 31, 7.0F, -1.0F, -1.5F, 1, 1, 3, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 8, 35, -8.0F, -1.0F, -1.5F, 1, 1, 3, 0.0F, false));

		legs1 = new ModelRenderer(this);
		legs1.setRotationPoint(0.0F, 16.5F, 0.0F);
		setRotationAngle(legs1, -0.7854F, 0.0F, 0.0F);
		legs1.cubeList.add(new ModelBox(legs1, 24, 0, -0.5F, -0.5F, -0.5F, 1, 1, 11, 0.0F, false));
		legs1.cubeList.add(new ModelBox(legs1, 4, 31, -0.5F, -0.5F, -0.5F, 1, 11, 1, 0.0F, false));

		legs2 = new ModelRenderer(this);
		legs2.setRotationPoint(0.0F, 16.5F, 0.0F);
		setRotationAngle(legs2, -0.7854F, -1.5708F, 0.0F);
		legs2.cubeList.add(new ModelBox(legs2, 0, 0, -0.5F, -0.5F, -0.5F, 1, 1, 11, 0.0F, false));
		legs2.cubeList.add(new ModelBox(legs2, 0, 31, -0.5F, -0.5F, -0.5F, 1, 11, 1, 0.0F, false));

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, 15.0F, 0.0F);
		head.cubeList.add(new ModelBox(head, 0, 12, -3.0F, -3.5F, -3.0F, 6, 4, 6, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 24, 12, -1.0F, -1.5F, -11.0F, 2, 2, 8, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 18, 22, 3.0F, -1.5F, -7.0F, 1, 1, 8, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 0, 22, -4.0F, -1.5F, -7.0F, 1, 1, 8, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		base.render(f5);
		legs1.render(f5);
		legs2.render(f5);
		head.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}