//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class ModelHook extends ModelBase {
	private final ModelRenderer bone;
	private final ModelRenderer a1c;
	private final ModelRenderer bone2;
	private final ModelRenderer a2c;
	private final ModelRenderer bone3;
	private final ModelRenderer a3c;
	private final ModelRenderer bone4;

	public ModelHook() {
		textureWidth = 64;
		textureHeight = 64;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 24.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 0, -3.0F, -16.0F, -3.0F, 6, 2, 6, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 24, 0, -2.0F, -17.0F, -2.0F, 4, 1, 4, 0.0F, false));

		a1c = new ModelRenderer(this);
		a1c.setRotationPoint(0.0F, 24.0F, 0.0F);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(2.5F, -14.5F, 0.5F);
		setRotationAngle(bone2, 0.0F, 0.0F, 0.7854F);
		a1c.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 0, 10, 0.5F, -0.5F, -0.5F, 7, 1, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 0, 16, 6.5F, 0.5F, -0.5F, 1, 5, 1, 0.0F, false));

		a2c = new ModelRenderer(this);
		a2c.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(a2c, 0.0F, -2.0944F, 0.0F);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(2.5F, -14.5F, 0.5F);
		setRotationAngle(bone3, 0.0F, 0.0F, 0.7854F);
		a2c.addChild(bone3);
		bone3.cubeList.add(new ModelBox(bone3, 16, 8, 0.5F, -0.5F, -0.5F, 7, 1, 1, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 20, 10, 6.5F, 0.5F, -0.5F, 1, 5, 1, 0.0F, false));

		a3c = new ModelRenderer(this);
		a3c.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(a3c, 0.0F, 2.0944F, 0.0F);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(2.5F, -14.5F, 0.5F);
		setRotationAngle(bone4, 0.0F, 0.0F, 0.7854F);
		a3c.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 0, 8, 0.5F, -0.5F, -0.5F, 7, 1, 1, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 16, 10, 6.5F, 0.5F, -0.5F, 1, 5, 1, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
		a1c.render(f5);
		a2c.render(f5);
		a3c.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}