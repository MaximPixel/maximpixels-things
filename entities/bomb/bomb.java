//Made with Blockbench
//Paste this code into your mod.

import org.lwjgl.opengl.GL11;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;

public class bomb extends ModelBase {
	private final ModelRenderer bone;

	public bomb() {
		textureWidth = 64;
		textureHeight = 64;

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 19.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 0, -3.0F, -6.0F, -7.0F, 6, 6, 11, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 3, -2.0F, -5.0F, 7.0F, 4, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 17, -2.0F, -5.0F, -8.0F, 4, 4, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 10, 14, 0.0F, -5.0F, 4.0F, 0, 4, 3, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 0, -2.0F, -3.0F, 4.0F, 4, 0, 3, 0.0F, false));
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
		bone.render(f5);
	}
	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}